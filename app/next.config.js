import { request, gql } from 'graphql-request'

export const getStaticProps = async () => {
  const query = gql`
    {
      test {
        title
      }
    }
  `;

  const data = await request(process.env.CMS_GRAPHQL_URL, query);

  return {
    props: data.test,
    revalidate: 10,
  };
};
